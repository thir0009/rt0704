from film import *
from datetime import datetime

class Videos:
    def __init__(self, nom, prenom, film = []):
        self.proprietaire = {'nom' : nom, 'prenom': prenom}
        self.date = datetime.now().strftime("%d/%m/%Y")
        self.films = film
    
    def toJson(self):
        return json.dumps(self.__dict__, indent=4)
    
    
