import json
import os
from videos import *

def read_name():
    res = {"videos":[]}
    res["videos"] = os.listdir("videos/")
    return res

'''
Création d'une vidéothèque.
@Param : 
    - file_name : le nom du fichier final
    - nom : le nom du propriétaire de la vidéothèque
    - prénom : le prénom du propriétaire de la vidéothèque
@Return :
    Retourne le fichier au format json du contenu du fichier videothque vide
'''
def create(file_name, nom, prenom):
    res = json.dumps({"Erreur" : "Impossible de créer la vidéothèque"})

    videos = None
    path_file = "videos/"+file_name+".json"

    if(not os.path.exists(path_file)):
        videos = Videos(nom, prenom)
        with open(path_file, "w+") as file:
            json_str = videos.toJson()
            file.write(json_str)
            res = json_str
    return res

'''
Recherche un film en particulier dans une vidéotheque
@Param :
    - file_name, le nom du fichier a traiter
    - film_name, le nom du film a rechercher
@Return:
    Si le film a été retrouver, il est retourné
'''
def read(file_name, film_name):
    res = json.dumps({"Erreur" : "Film introuvable"})
    
    path_file = "videos/"+file_name+".json"
    if(os.path.isfile(path_file)):
        with open(path_file, "r") as file:
            video_json = json.load(file)
            for film in video_json['films']:
                if(film['titre'] == film_name):
                    res = film
    return res

'''
Lit le contenu d'une videothèque
@Param : 
    - args = all : retourne l'integralité des films contenu dans la videotheque
    - args = name_film :  retourne le film correspondant a name_film si il existe
@Return :
    - Le contenu en fonction des parametres
'''
def read_all(file_name):
    path_file = "videos/"+file_name+".json"
    res = {"films" : []}
    if(os.path.isfile(path_file)):
        with open(path_file, "r") as file:
            video_json = json.load(file)
            res["films"] = video_json["films"]
            return res
    return json.dumps({"Erreur" : "Videotheque introuvable"})


'''
Ajout d'un film dans la videotheque
@Param :
    - file_name, le nom du fichier a modifier
    - titre, le nom du film
    - annee, l'année de prodution du film
    - nom_real, le nom du réalisateur
    - prenom_real, le prenom du realisateur
    - list_acteur, la liste des acteurs presents dans le film
'''
def update(file_name, titre, annee, nom_real, prenom_real, nom_acteur1, nom_acteur2, nom_acteur3, prenom_acteur1, prenom_acteur2, prenom_acteur3):
    tab_acteur_init = []
    if(nom_acteur1 != "" and prenom_acteur1 != ""):
        tab_acteur_init.append({"nom": nom_acteur1, "prenom": prenom_acteur1})
    if(nom_acteur2 != "" and prenom_acteur2 != ""):
        tab_acteur_init.append({"nom": nom_acteur2, "prenom": prenom_acteur2})
    if(nom_acteur3 != "" and prenom_acteur3 != ""):
        tab_acteur_init.append({"nom": nom_acteur3, "prenom": prenom_acteur3})
    new_film = film(titre, annee, nom_real, prenom_real, tab_acteur_init)

    path_file = "videos/"+file_name+".json"
    if(os.path.isfile(path_file)):
        with open(path_file, "r") as file:
            video_json = json.load(file)
            video_json['films'].append(new_film.__dict__)
            video_json_str = json.dumps(video_json, indent=4)

        with open(path_file, "w") as file:
            file.write(video_json_str)
        return json.dumps({"Succes" : "Ajout"})
    return json.dumps({"Erreur" : "La videotheque est introuvable"})

'''
Suppression d'une videotheque
@Param :
    - file_name, le nom du fichier a supprimer
@Return :
    Un string en fonction d'echec de la suppression ou de succes.
'''
def delete(file_name):
    res = json.dumps({"Erreur" : "La videotheque a supprimer n'existe pas"})

    path_file = "videos/"+file_name+".json"
    if(os.path.exists(path_file)):
        os.remove(path_file)
        res = json.dumps({"Succes" : "Le fihcier a ete supprimer"})
    return res

'''
Suppression d'un film en particulier
@Param :
    - file_name, le nom du fichier a traiter
    - titre, le titre du film a supprimer
@Return :
    Un strinf en fonction de l'echec ou du succes de la suppression.
'''
def delete_film(file_name, titre):
    res = json.dumps({"Erreur" : "Impossible de supprimer ce film"})
    path_file = "videos/"+file_name+".json"

    if(os.path.exists(path_file)):
        with open(path_file, "r") as file:
            video_json = json.load(file)
        for idx, film in enumerate(video_json['films']):
            if titre == film['titre']:
                video_json['films'].pop(idx)
                video_json_str = json.dumps(video_json, indent=4)
                with open(path_file, "w") as file:
                    file.write(video_json_str)
                res = json.dumps({"Succes" : "Film supprimer avec succes"})
    return res

'''
Lit tout les films dans lequel un acteur donné joue
@Param :
    - file_name, le nom du fichier a traiter
    - nom_acteur, le nom de l'acteur a rechercher
@Return :
    Une liste de film dans lequel l'acteur passé en param joue
'''
def read_actor(file_name, nom_acteur):
    res = {'films' : []}
    path_file = "videos/"+file_name+".json"
    if(os.path.exists(path_file)):
        with open(path_file, "r") as file:
            video_json = json.load(file)
            for film in video_json['films']:
                for acteur in film['acteurs']:
                    if(acteur['nom'].lower() == nom_acteur.lower()):
                        res['films'].append(film)
                        return res
    else:
        return json.dumps({"Erreur" : "La videotheque est introuvable"})
    
def modify(file_name, titre_init, titre, annee, nom_real, prenom_real, nom_acteur1, nom_acteur2, nom_acteur3, prenom_acteur1, prenom_acteur2, prenom_acteur3):
    res = delete_film(file_name, titre_init)
    res_json = json.loads(res)
    if(res_json["Succes"]):
        return update(file_name, titre, annee, nom_real, prenom_real, nom_acteur1, nom_acteur2, nom_acteur3, prenom_acteur1, prenom_acteur2, prenom_acteur3)
    

