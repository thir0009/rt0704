from flask import Flask
from flask import request
from crud_json import *

app = Flask(__name__)

'''
Recupere l'ensemble des videotheque existante
Return:
    - json contenant la liste des videotheque existante
'''
@app.route('/', methods = ['GET', 'POST', 'PUT', 'DELETE'])
def get_all_videotheque():
    return read_name()


'''
creation d'une videotheque
arguments du form : 
    - file_name, le nom du fichier final
    -  nom, le nom du propriétaire de la videotheque
    -  prenom, le prenom du propriétaire de la videotheque
Réponse de la requete :
    Le contenu du fichier json nouvelle créer et vide
'''
@app.route('/video', methods = ['POST'])
def add_videotheque():
    nom = request.form['nom']
    prenom = request.form['prenom']
    file_name = request.form['file_name']
    res = create(file_name, nom, prenom)
    return res

'''
Supression d'une videotheque
'''
@app.route('/video/<string:nom>', methods = ['DELETE'])
def del_videotheque(nom):
    res = delete(nom)
    return res

'''
Affichage de l'ensemble des films d'une videotheque
'''
@app.route('/video/<string:nom>/films', methods = ['GET'])
def get_films(nom):
    res = read_all(nom)
    return res

'''
Recherche d'un film
'''
@app.route('/video/<string:nom>/films/<string:titre>', methods = ['GET'])
def search_film(nom,titre):
    res = read(nom, titre)
    return res

'''
Ajout d'un film
'''
@app.route('/video/<string:nom>/films', methods = ['POST'])
def add_film(nom):
    titre = request.form['titre']
    annee = request.form['annee']
    nom_real = request.form['nom_real']
    prenom_real = request.form['prenom_real']
    nom_acteur1 = request.form['nom_acteur1']
    nom_acteur2 = request.form['nom_acteur2']
    nom_acteur3 = request.form['nom_acteur3']
    prenom_acteur1 = request.form['prenom_acteur1']
    prenom_acteur2 = request.form['prenom_acteur2']
    prenom_acteur3 = request.form['prenom_acteur3']
    res = update(nom, titre, annee, nom_real, prenom_real, nom_acteur1, nom_acteur2, nom_acteur3, prenom_acteur1, prenom_acteur2, prenom_acteur3)
    return res

'''
Suppression d'un film
'''
@app.route('/video/<string:nom>/films/<string:titre>', methods = ['DELETE'])
def del_film(nom, titre):
    res = delete_film(nom, titre)
    return res

'''
Recherche des films d'un acteur
'''
@app.route('/video/<string:nom>/films/acteur/<string:nom_acteur>', methods = ['GET'])
def get_film_from_actor(nom, nom_acteur):
    res = read_actor(nom, nom_acteur)
    return res

'''
Modification d'un film
'''
@app.route('/video/<string:nom>/films/modify/<string:titre>', methods = ['PUT'])
def modif_film(nom, titre):
    print(nom, titre)
    titre_init = request.form["titre_init"]
    titre = request.form['new_titre']
    annee = request.form['annee']
    nom_real = request.form['nom_real']
    prenom_real = request.form['prenom_real']
    nom_acteur1 = request.form['nom_acteur1']
    nom_acteur2 = request.form['nom_acteur2']
    nom_acteur3 = request.form['nom_acteur3']
    prenom_acteur1 = request.form['prenom_acteur1']
    prenom_acteur2 = request.form['prenom_acteur2']
    prenom_acteur3 = request.form['prenom_acteur3']
    res = modify(nom, titre_init, titre, annee, nom_real, prenom_real, nom_acteur1, nom_acteur2, nom_acteur3, prenom_acteur1, prenom_acteur2, prenom_acteur3)
    return res 

app.run(debug =True)