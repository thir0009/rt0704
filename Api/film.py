from acteur import *

class film:
    def __init__(self, titre, annee, nom_real, prenom_real, tab_acteur):
        self.titre = titre
        self.annee = int(annee)
        self.realisateur = {"nom": nom_real, "prenom": prenom_real}
        self.acteurs = tab_acteur
    
    def toJson(self):
        return json.dumps(self.__dict__)