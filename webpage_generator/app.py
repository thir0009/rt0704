from flask import Flask
from flask import request
from flask import render_template, flash
import jinja2
import json
import requests

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
rest_url = "http://10.11.5.33:5000"

@app.route('/', methods = ['GET', 'POST', 'PUT', 'DELETE'])
def get_all_videotheque():
    url = rest_url+"/"
    r = requests.get(url)
    response_json = r.json()
    tab_video = []
    for video in response_json["videos"]:
        tab_video.append(video.rsplit('.', 1)[0])
    return render_template("home.html", data1=tab_video)

@app.route('/video', methods = ['POST'])
def add_videotheque():
    nom = request.form['nom']
    prenom = request.form['prenom']
    if(nom != "" and prenom != ""):
        data = {'nom' : nom, 'prenom': prenom, 'file_name':nom}
        url = rest_url+"/video"
        r = requests.post(url, data)
        response_json = r.json()
        return get_all_videotheque()

    return "Erreur de parametre"

@app.route('/video/<string:nom>/films', methods = ['GET'])
def get_films(nom):

    if(nom != ""):
        url = rest_url+"/video/"+nom+"/films"
        r = requests.get(url)
        response_json = r.json()
        return render_template("films.html", data1=response_json, data2=nom)

@app.route('/video/<string:nom>/films', methods = ['POST'])
def add_film(nom):
    titre = request.form['titre']
    annee = request.form['annee']
    nom_real = request.form['nom_real']
    prenom_real = request.form['prenom_real']
    nom_acteur1 = request.form['nom_acteur1']
    nom_acteur2 = request.form['nom_acteur2']
    nom_acteur3 = request.form['nom_acteur3']
    prenom_acteur1 = request.form['prenom_acteur1']
    prenom_acteur2 = request.form['prenom_acteur2']
    prenom_acteur3 = request.form['prenom_acteur3']
    url = rest_url+"/video/"+nom+"/films"
    if(titre and annee and nom_real and prenom_real):
        data = {"titre": titre, "annee": annee, "nom_real": nom_real, "prenom_real": prenom_real, "file_name":nom}
        if(nom_acteur1 and prenom_acteur1):
            data["nom_acteur1"] = nom_acteur1
            data["prenom_acteur1"] = prenom_acteur1
        if(nom_acteur2 and prenom_acteur2):
            data["nom_acteur2"] = nom_acteur2
            data["prenom_acteur2"] = prenom_acteur2
        if(nom_acteur3 and prenom_acteur3):
            data["nom_acteur3"] = nom_acteur3
            data["prenom_acteur3"] = prenom_acteur3
        r = requests.post(url, data)
        return get_films(nom)
    else:
        pass

@app.route('/video/<string:nom>/delete', methods=['POST'])
def delete_video(nom):
    url = rest_url+"/video/"+nom
    r = requests.delete(url)
    return get_all_videotheque()

@app.route('/video/<nom>/delete/<titre>', methods=['POST'])
def delete_film(nom, titre):
    url = rest_url+"/video/"+nom+"/films/"+titre
    r = requests.delete(url)
    return get_films(nom)
    

@app.route('/video/<string:nom>/films/search', methods=['POST'])
def search_film(nom):
    titre = request.form["titre"]
    if(titre):
        url = rest_url+"/video/"+nom+"/films/"+titre
        r = requests.get(url)
        if(not "Erreur" in r.json() and r.json()):
            response_json = {"films" : []}
            response_json["films"].append(r.json())
            return render_template("films.html", data1=response_json, data2=nom)
    return get_films(nom)

@app.route('/video/<string:nom>/films/<string:titre>', methods=['POST'])
def modify_film_p(nom, titre):
    url = rest_url+"/video/"+nom+"/films/"+titre
    r = requests.get(url)
    return render_template("modif_film.html", data1=r.json(), data2=nom)

@app.route('/video/<string:nom>/films/modify/<string:titre>', methods=['POST'])
def modify_film(nom, titre):
    url = rest_url+"/video/"+nom+"/films/modify/"+titre
    new_titre = request.form['titre']
    annee = request.form['annee']
    nom_real = request.form['nom_real']
    prenom_real = request.form['prenom_real']
    nom_acteur1 = request.form['nom_acteur1']
    nom_acteur2 = request.form['nom_acteur2']
    nom_acteur3 = request.form['nom_acteur3']
    prenom_acteur1 = request.form['prenom_acteur1']
    prenom_acteur2 = request.form['prenom_acteur2']
    prenom_acteur3 = request.form['prenom_acteur3']
    if(titre and annee and nom_real and prenom_real):
        data = {"titre_init" : titre, "new_titre": new_titre, "annee": annee, "nom_real": nom_real, "prenom_real": prenom_real, "file_name":nom}
        if(nom_acteur1 and prenom_acteur1):
            data["nom_acteur1"] = nom_acteur1
            data["prenom_acteur1"] = prenom_acteur1
        if(nom_acteur2 and prenom_acteur2):
            data["nom_acteur2"] = nom_acteur2
            data["prenom_acteur2"] = prenom_acteur2
        if(nom_acteur3 and prenom_acteur3):
            data["nom_acteur3"] = nom_acteur3
            data["prenom_acteur3"] = prenom_acteur3
    r = requests.put(url, data)
    return get_films(nom)
    


app.run(debug=True)